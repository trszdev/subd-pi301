-- MS SQL 2014
USE master;
DROP DATABASE EmilRakhimyanov;
CREATE DATABASE EmilRakhimyanov;
USE EmilRakhimyanov;

CREATE TABLE Stations (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80),
    address_ NVARCHAR(80) DEFAULT 'Россия, г.'
);

CREATE TABLE MeasureUnits (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80),
    short_name NVARCHAR(20)
);

CREATE TABLE MeasureTypes (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80),
    unit INT
);

CREATE TABLE Measurings (
    id INT PRIMARY KEY NOT NULL,
    station INT,
    mtype INT,
    VALUE REAL,
    measureTime DATETIME
);

ALTER TABLE Measurings ADD
    CONSTRAINT station FOREIGN KEY (station)
    REFERENCES Stations (id);
 
ALTER TABLE Measurings ADD
    CONSTRAINT mtype FOREIGN KEY (mtype)
    REFERENCES MeasureTypes (id);
 
ALTER TABLE MeasureTypes ADD
    CONSTRAINT unit FOREIGN KEY (unit)
    REFERENCES MeasureUnits (id);

INSERT INTO MeasureUnits VALUES (1, 'Percentage', '%');
INSERT INTO MeasureUnits VALUES (2, 'Newtons', 'N');
INSERT INTO MeasureUnits VALUES (3, 'Celsium', 'c');

INSERT INTO MeasureTypes VALUES (1, 'Humidity', 1);
INSERT INTO MeasureTypes VALUES (2, 'Pressure', 2);
INSERT INTO MeasureTypes VALUES (3, 'Temperature', 3);

INSERT INTO Stations VALUES (1, 'EKB', '');
INSERT INTO Stations VALUES (2, 'MSK', '');

INSERT INTO Measurings VALUES (1, 1, 3, 33, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (2, 1, 3, 34, '1945-01-14 18:56:00');
INSERT INTO Measurings VALUES (3, 1, 3, 35, '1945-01-15 18:56:00');
INSERT INTO Measurings VALUES (4, 1, 3, 36, '1945-01-16 18:56:00');
INSERT INTO Measurings VALUES (5, 1, 3, 37, '1945-01-17 18:56:00');
INSERT INTO Measurings VALUES (6, 1, 3, 38, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (7, 1, 3, 39, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (8, 1, 3, 40, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (9, 1, 3, 41, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (10, 1, 3, 42, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (11, 1, 3, 43, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (12, 1, 3, 44, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (13, 1, 3, 36, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (14, 1, 3, 37, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (15, 1, 3, 38, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (16, 1, 3, 39, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (17, 1, 3, 40, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (18, 2, 3, 41, '1945-01-21 18:56:00');
INSERT INTO Measurings VALUES (19, 2, 3, 42, '1945-01-1 18:56:00');
INSERT INTO Measurings VALUES (20, 2, 3, 43, '1945-01-13 18:56:00');
INSERT INTO Measurings VALUES (21, 1, 2, 10, '1945-01-29 18:56:00');

SELECT DISTINCT measureTime FROM Measurings ORDER BY measureTime DESC;
SELECT mtype, MIN(VALUE), MAX(VALUE) FROM Measurings GROUP BY mtype;
SELECT COUNT(*) FROM Measurings;
SELECT * FROM Measurings WHERE mtype IN (
    SELECT id FROM MeasureTypes WHERE unit IN (
        SELECT id FROM MeasureUnits WHERE name = 'Celsium'
    )
);

SELECT measureTime, mtype FROM Measurings AS M WHERE VALUE IN (
    SELECT MIN(VALUE) FROM Measurings AS M2 WHERE M2.mtype = M.mtype
);


SELECT FORMAT(measureTime,'dd-MM-yyyy') AS 'Время', AVG(VALUE) AS 'Среднее значение' FROM Measurings
    GROUP BY FORMAT(measureTime,'dd-MM-yyyy');

SELECT
    mName AS 'Тип измерения',
    uName AS 'Единица измерения',
    AVG(VALUE) AS 'Среднее значение',
    measureDate AS 'День',
    sName AS 'Станция'
    FROM (
        SELECT
            MeasureTypes.name AS mName,
            VALUE, MeasureUnits.name AS uName,
            FORMAT(measureTime, 'dd-MM-yyyy') AS measureDate,
            Stations.name AS sName
            FROM Measurings
                LEFT JOIN MeasureTypes ON mtype=MeasureTypes.id
                LEFT JOIN Stations ON station=Stations.id
                LEFT JOIN MeasureUnits ON MeasureTypes.unit=MeasureUnits.id
) AS sel GROUP BY measureDate, sName, mName, uName;