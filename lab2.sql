-- MS SQL 2014
USE master;
DROP DATABASE EmilRakhimyanov;
CREATE DATABASE EmilRakhimyanov;
USE EmilRakhimyanov;

CREATE TABLE Posts (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80)
);

CREATE TABLE Regions (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80),
    CONSTRAINT CheckRegion CHECK (
        id BETWEEN 100 AND 199 OR
        id BETWEEN 1 AND 99 OR
        id BETWEEN 700 AND 799
    )
);

CREATE TABLE Cars (
    id INT PRIMARY KEY NOT NULL,
    name NVARCHAR(80),
    regionId INT,
    color NVARCHAR(80),
    plateNumber NCHAR(6),
    driverName NVARCHAR(80),
    CONSTRAINT CheckPlate CHECK (
        plateNumber LIKE '%[хуеакнаросмт][0-9][0-9][0-9][хуеакнаросмт][хуеакнаросмт]%'
    )
);

CREATE TABLE PostRecords (
    id INT PRIMARY KEY NOT NULL,
    postId INT,
    carId INT,
    isArriving BIT,
    moment TIME
);

ALTER TABLE PostRecords ADD
    CONSTRAINT carId FOREIGN KEY (carId)
    REFERENCES Cars (id);

ALTER TABLE PostRecords ADD
    CONSTRAINT postId FOREIGN KEY (postId)
    REFERENCES Posts (id);

ALTER TABLE Cars ADD
    CONSTRAINT regionId FOREIGN KEY (regionId)
    REFERENCES Regions (id);

GO

CREATE VIEW Transit AS
    SELECT DISTINCT A.carId FROM PostRecords A
    JOIN Cars C ON C.id = A.carId
    JOIN Regions R ON R.id = C.regionId
    CROSS JOIN PostRecords B
    WHERE
        A.moment < B.moment AND
        A.carId = B.carId AND
        A.postId != B.postId AND
        R.name != 'Свердловская область' AND
        A.isArriving = 1 AND B.isArriving = 0;
GO

CREATE VIEW NonResident AS
    SELECT DISTINCT A.carId FROM PostRecords A
    JOIN Cars C ON C.id = A.carId
    JOIN Regions R ON R.id = C.regionId
    CROSS JOIN PostRecords B
    WHERE
        A.moment < B.moment AND
        A.carId = B.carId AND
        A.postId = B.postId AND
        A.isArriving = 1 AND B.isArriving = 0;
GO

CREATE VIEW Resident AS
    SELECT DISTINCT A.carId FROM PostRecords A
    JOIN Cars C ON C.id = A.carId
    JOIN Regions R ON R.id = C.regionId
    CROSS JOIN PostRecords B
    WHERE
        A.moment < B.moment AND
        A.carId = B.carId AND
        R.name = 'Свердловская область' AND
        A.isArriving = 1 AND B.isArriving = 0;
GO

CREATE VIEW Others AS
    SELECT DISTINCT A.carId FROM PostRecords A
    EXCEPT SELECT R.carId FROM Resident R
    EXCEPT SELECT T.carId FROM Transit T
    EXCEPT SELECT NR.carId FROM NonResident NR
GO

CREATE TRIGGER NotAlwaysEntering ON PostRecords
    AFTER INSERT AS
    BEGIN
        DECLARE @isArriving BIT, @carId INT
        SET @isArriving = (SELECT isArriving FROM INSERTED)
        SET @carId = (SELECT carId FROM INSERTED)
        IF EXISTS(SELECT * FROM (
                SELECT * FROM PostRecords WHERE carId = @carId ORDER BY id DESC
                    OFFSET 1 ROWS FETCH NEXT 1 ROWS ONLY
            ) AS X WHERE @isArriving = isArriving
        )
        BEGIN
            ROLLBACK
            RAISERROR ('Въезжает/выезжает дважды', 16, 1);
        END
    END
GO

INSERT INTO Regions VALUES (96, 'Свердловская область');
INSERT INTO Regions VALUES (66, 'Свердловская область');
INSERT INTO Regions VALUES (50, 'Московская область');
INSERT INTO Regions VALUES (150, 'Московская область');
INSERT INTO Regions VALUES (128, 'Псковская область');
INSERT INTO Cars VALUES (1, 'Газель', 66, 'Black', 'х999хх', 'Иванов');
INSERT INTO Cars VALUES (2, 'Лада', 96, 'White', 'у123уу', 'Кузнецов');
INSERT INTO Cars VALUES (3, 'Toyota', 50, 'Black', 'е789ее', 'Brown');
INSERT INTO Cars VALUES (4, 'Jeep', 150, 'Green', 'а765аа', 'Smith');
INSERT INTO Cars VALUES (5, 'Bugatti', 128, 'Black', 'х111ух', 'Ivanov');
INSERT INTO Cars VALUES (6, 'Лада', 128, 'Red', 'х161уе', 'Smirnov');
INSERT INTO Posts VALUES (1, 'Пост #1');
INSERT INTO Posts VALUES (2, 'Пост #2');
INSERT INTO Posts VALUES (3, 'Пост #3');
INSERT INTO Posts VALUES (4, 'Пост #4');
 
-- Resident:
INSERT INTO PostRecords VALUES (1, 1, 1, 1, '18:56:00');
INSERT INTO PostRecords VALUES (2, 2, 1, 0, '18:56:01');
-- Other:
INSERT INTO PostRecords VALUES (3, 2, 2, 1, '18:56:02');
INSERT INTO PostRecords VALUES (4, 2, 3, 1, '18:56:02');
INSERT INTO PostRecords VALUES (5, 1, 3, 0, '18:56:02');
INSERT INTO PostRecords VALUES (6, 2, 3, 1, '18:56:02');
-- NonResident:
INSERT INTO PostRecords VALUES (7, 2, 4, 1, '18:56:02');
INSERT INTO PostRecords VALUES (8, 2, 4, 0, '18:57:03');
-- Transit
INSERT INTO PostRecords VALUES (9, 1, 5, 1, '18:56:04');
INSERT INTO PostRecords VALUES (10, 2, 5, 0, '18:57:05');
 
 
SELECT carId AS 'ID Транзитных авто' FROM Transit;
SELECT carId AS 'ID Местных авто' FROM Resident;
SELECT carId AS 'ID Иногородних авто' FROM NonResident;
 
SELECT COUNT(DISTINCT carId) AS 'Количество всех авто' FROM PostRecords;
SELECT COUNT(carId) AS 'Количество прочих авто' FROM Others;
 
SELECT
    C.name AS 'Авто', C.driverName AS 'Водитель', C.plateNumber AS 'Номерной знак', C.color AS 'Цвет', R.name AS 'Регион'
    FROM PostRecords A
    JOIN Cars C ON A.carId = C.id
    JOIN Regions R ON C.regionId = R.id
    GROUP BY A.carId, C.name, C.driverName, C.plateNumber, C.color, R.name
    ORDER BY R.name;