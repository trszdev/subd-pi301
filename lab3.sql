-- MS SQL 2014
USE master;
DROP DATABASE EmilRakhimyanov;
CREATE DATABASE EmilRakhimyanov;
USE EmilRakhimyanov;

CREATE TABLE Cities (id INT PRIMARY KEY NOT NULL, name NVARCHAR(80));
CREATE TABLE Food (id INT PRIMARY KEY NOT NULL, name NVARCHAR(80));
CREATE TABLE Bag (id INT PRIMARY KEY NOT NULL, foodId INT, amount INT);
CREATE TABLE FoodPrices (
    id INT PRIMARY KEY NOT NULL,
    foodId INT,
    cityId INT,
    avgPrice FLOAT
);

ALTER TABLE Bag ADD
    CONSTRAINT bagFoodId FOREIGN KEY (foodId)
    REFERENCES Food (id);

ALTER TABLE FoodPrices ADD
    CONSTRAINT foodId FOREIGN KEY (foodId)
    REFERENCES Food (id);

ALTER TABLE FoodPrices ADD
    CONSTRAINT cityId FOREIGN KEY (cityId)
    REFERENCES Cities (id);

INSERT INTO Cities VALUES (1, N'Екатеринбург')
INSERT INTO Cities VALUES (2, N'Челябинск')
INSERT INTO Cities VALUES (3, N'Уфа')
INSERT INTO Cities VALUES (4, N'Магнитогорск')
INSERT INTO Cities VALUES (5, N'Тюмень')

INSERT INTO Food VALUES (1, N'Яблоко')
INSERT INTO Food VALUES (2, N'Груша')
INSERT INTO Food VALUES (3, N'Банан')
INSERT INTO Food VALUES (4, N'Виноград')

INSERT INTO FoodPrices VALUES (1, 1, 1, 5.2)
INSERT INTO FoodPrices VALUES (2, 1, 2, 6.2)
INSERT INTO FoodPrices VALUES (3, 1, 3, 10)
INSERT INTO FoodPrices VALUES (4, 1, 4, 38)
INSERT INTO FoodPrices VALUES (5, 1, 5, 0.1)
INSERT INTO FoodPrices VALUES (6, 2, 1, 85.2)
INSERT INTO FoodPrices VALUES (7, 2, 2, 86.2)
INSERT INTO FoodPrices VALUES (8, 2, 3, 810)
INSERT INTO FoodPrices VALUES (9, 2, 4, 838)
INSERT INTO FoodPrices VALUES (10, 2, 5, 80.1)

INSERT INTO Bag VALUES (1, 1, 2)
INSERT INTO Bag VALUES (2, 2, 4)
GO

CREATE FUNCTION GetCitiesQuoted() RETURNS NVARCHAR(MAX)
BEGIN
	DECLARE @result NVARCHAR(MAX)
	SELECT @result = COALESCE(@result + ', ', '') + QUOTENAME(C.name) from Cities C
	RETURN @result
END
GO

CREATE FUNCTION GetFoodQuery(@addition NVARCHAR(MAX)) RETURNS NVARCHAR(MAX)
BEGIN
	RETURN FORMATMESSAGE(N'
		WITH CityProducts AS (
			SELECT C.name AS City, FP.avgPrice + (%s) AS Price, F.name as ''Продукт''
			FROM FoodPrices FP 
			JOIN Cities C ON FP.cityId = C.id 
			JOIN Food F ON F.id = FP.foodId
			JOIN MinMaxPrices MMP ON MMP.id = FP.foodId
		)
		SELECT * FROM CityProducts
		PIVOT (MIN(Price) FOR City in (%s)) AS Pivot1
	', @addition, dbo.GetCitiesQuoted())
END
GO

CREATE VIEW MinMaxPrices AS
	SELECT 
		FP.foodId as id, 
		MIN(FP.avgPrice) AS MinPrice, 
		MAX(FP.avgPrice) AS MaxPrice 
	FROM FoodPrices FP
	GROUP BY FP.foodId
GO

CREATE FUNCTION GetBagPrice(@cityName NVARCHAR(MAX)) RETURNS FLOAT AS
BEGIN
	DECLARE @result FLOAT
	DECLARE @tempTable table (totalProductPrice FLOAT)

	INSERT INTO @tempTable 
	SELECT B.amount * FP.avgPrice AS ProductPrice FROM Bag B
		RIGHT JOIN FoodPrices FP ON FP.foodId = B.foodId
		JOIN Cities C ON C.id = FP.cityId
		WHERE C.name = @cityName

	SELECT @result = SUM(totalProductPrice) from @tempTable
	RETURN @result
END
GO

SELECT dbo.GetBagPrice(N'Екатеринбург') AS N'Стоимость сумки'

DECLARE @Average NVARCHAR(MAX), @AverageSubMin NVARCHAR(MAX), @AverageSubMax NVARCHAR(MAX)
SET @Average = dbo.GetFoodQuery('0')
SET @AverageSubMin = dbo.GetFoodQuery('-MMP.MinPrice')
SET @AverageSubMax = dbo.GetFoodQuery('-MMP.MaxPrice')
EXECUTE(@Average)
EXECUTE(@AverageSubMin)
EXECUTE(@AverageSubMax)