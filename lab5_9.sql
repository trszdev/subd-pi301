﻿-- MS SQL 2014
USE master
DROP DATABASE EmilRakhimyanov
CREATE DATABASE EmilRakhimyanov
USE EmilRakhimyanov


CREATE TABLE Persons (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    name NVARCHAR(80),
    secondName NVARCHAR(80),
    thirdName NVARCHAR(80)
)

CREATE TABLE Clients (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    personId INT FOREIGN KEY REFERENCES Persons(id),
    homeAddress NVARCHAR(80),
    phoneNumber NVARCHAR(80)
)

CREATE TABLE Publishers (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    name NVARCHAR(80),
    publishYear INT
)

CREATE TABLE Books (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    name NVARCHAR(80),
    publisherId INT FOREIGN KEY REFERENCES Publishers(id),
    pagesCount INT,
    price FLOAT,
    availableCopies INT
)

CREATE TABLE Authors (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    personId INT FOREIGN KEY REFERENCES Persons(id),
    bookId INT FOREIGN KEY REFERENCES Books(id)
)

CREATE TABLE BookOrders (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    clientId INT FOREIGN KEY REFERENCES Clients(id),
    bookId INT FOREIGN KEY REFERENCES Books(id),
    dateTaken DATETIME,
    dateReturn DATETIME,
    CONSTRAINT CheckDateInterval CHECK (
        DATEDIFF(minute, dateTaken, dateReturn) >= 0 
    )
)

CREATE TABLE BookReservations (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    clientId INT FOREIGN KEY REFERENCES Clients(id),
    bookId INT FOREIGN KEY REFERENCES Books(id),
    dateTaken DATETIME
)

GO
CREATE FUNCTION GetClientBooks(@clientId INT)
RETURNS @result TABLE (
    id INT,
    name NVARCHAR(80),
    publisherId INT,
    pagesCount INT,
    price FLOAT,
    availableCopies INT
) AS
BEGIN
    INSERT @result SELECT B.* FROM BookOrders BO 
        JOIN Books B ON B.id = BO.bookId
        JOIN Clients C ON C.id = BO.clientId
        WHERE BO.clientId = @clientId
    RETURN
END
GO

GO
CREATE FUNCTION GetBookClients(@bookId INT)
RETURNS @result TABLE (
    id INT,
    personId INT,
    homeAddress NVARCHAR(80),
    phoneNumber NVARCHAR(80)
) AS
BEGIN
    INSERT @result SELECT C.* FROM BookOrders BO 
        JOIN Books B ON B.id = BO.bookId
        JOIN Clients C ON C.id = BO.clientId
        WHERE BO.bookId = @bookId AND DATEDIFF(minute, GETDATE(), BO.dateReturn) >= 0
    RETURN
END
GO

GO
CREATE FUNCTION GetBookwormClient(@from DATETIME, @to DATETIME)
RETURNS @result TABLE (
    id INT,
    personId INT,
    homeAddress NVARCHAR(80),
    phoneNumber NVARCHAR(80)
) AS
BEGIN
    WITH BookOrderFrequency AS (
        SELECT clientId, COUNT(clientId) as cnt FROM BookOrders
        WHERE DATEDIFF(MINUTE, @from, dateReturn) >= 0 AND DATEDIFF(MINUTE, @to, dateReturn) <= 0
        GROUP BY clientId 
    ) INSERT @result 
        SELECT TOP 1 C.* FROM Clients C
        JOIN BookOrderFrequency BOF ON BOF.clientId = C.id
        ORDER BY BOF.cnt DESC
    RETURN
END
GO


GO
CREATE FUNCTION GetBookWithMaxCopies()
RETURNS @result TABLE (
    id INT,
    name NVARCHAR(80),
    publisherId INT,
    pagesCount INT,
    price FLOAT,
    availableCopies INT
) AS
BEGIN
    INSERT @result SELECT TOP 1 * FROM Books ORDER BY availableCopies DESC
    RETURN 
END
GO


GO
CREATE FUNCTION GetBookWithMaxReservations()
RETURNS @result TABLE (
    id INT,
    name NVARCHAR(80),
    publisherId INT,
    pagesCount INT,
    price FLOAT,
    availableCopies INT
) AS
BEGIN
    WITH BookReservationsFrequency AS (
        SELECT bookId, COUNT(bookId) as cnt FROM BookReservations
        GROUP BY bookId 
    ) INSERT @result 
        SELECT TOP 1 B.* FROM Books B
        JOIN BookReservationsFrequency BRF ON BRF.bookId = B.id
        ORDER BY BRF.cnt DESC
    RETURN 
END
GO


GO
CREATE FUNCTION GetBookWithMaxClientsOnInterval(@from DATETIME, @to DATETIME)
RETURNS @result TABLE (
    id INT,
    name NVARCHAR(80),
    publisherId INT,
    pagesCount INT,
    price FLOAT,
    availableCopies INT
) AS
BEGIN
    WITH BookOrderFrequency AS (
        SELECT bookId, COUNT(bookId) as cnt FROM BookOrders
        WHERE DATEDIFF(MINUTE, @from, dateReturn) >= 0 AND DATEDIFF(MINUTE, @to, dateReturn) <= 0
        GROUP BY bookId 
    ) INSERT @result 
        SELECT TOP 1 B.* FROM Books B
        JOIN BookOrderFrequency BOF ON BOF.bookId = B.id
        ORDER BY BOF.cnt DESC
    RETURN
END
GO

GO
CREATE TRIGGER NotTakingSameBookTwice ON BookOrders
    AFTER INSERT AS
    BEGIN
        DECLARE @dateReturn DATETIME, @dateTaken DATETIME, @bookId INT
        SELECT @bookId = bookId, @dateReturn = dateReturn, @dateTaken = dateTaken FROM INSERTED
        IF ((SELECT COUNT(*) FROM BookOrders WHERE bookId = @bookId AND
            DATEDIFF(MINUTE, @dateTaken, dateReturn) >= 0 ) > 1)
        BEGIN
            ROLLBACK
            RAISERROR (N'Книгу нельзя взять дважды', 16, 1)
        END
    END
GO

INSERT Publishers VALUES
    (N'Издатель1', 1945),
    (N'Издатель2', 1946),
    (N'Издатель1', 1947)

INSERT Books VALUES
    (N'Книга1', 1, 300, 160.5, 10000),
    (N'Книга2', 2, 300, 160.5, 10000),
    (N'Книга3', 1, 300, 160.5, 20000),
    (N'Книга4', 3, 300, 160.5, 10000),
    (N'Книга5', 1, 300, 160.5, 10000),
    (N'Книга6', 1, 300, 160.5, 10000)

INSERT Persons VALUES 
    (N'Николай', N'Гоголь', N'Васильевич'),
    (N'Фёдор', N'Достоевский', N'Михайлович'),
    (N'Грибоедов', N'Александр', N'Сергеевич'),
    (N'Иван', N'Иванов', N'Иванович'),
    (N'Дмитрий', N'Иванов', N'Иванович')

INSERT Clients VALUES
    (3, N'улица Ленина', '8-800-555-35-35'),
    (4, N'улица Карла-Маркса', '8-900-444-35-35')

INSERT Authors VALUES
    (1, 1),
    (1, 2),
    (1, 3),
    (2, 3),
    (2, 4),
    (2, 5),
    (2, 6)

-- Вставлять по одному, чтобы сработал триггер
-- Если выдаст ошибку переполнения даты, попробуйте поменять местами год, месяц и день
INSERT BookOrders VALUES (1, 1, '1945-01-13 18:56:00', '2079-01-13 18:56:00')
INSERT BookOrders VALUES (1, 2, '1945-01-13 18:56:00', '1946-01-13 18:56:00')
INSERT BookOrders VALUES (2, 1, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (2, 6, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (2, 5, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (2, 4, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (2, 3, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (1, 3, '1945-01-13 18:56:00', '1946-01-13 18:56:00')
INSERT BookOrders VALUES (1, 4, '2000-01-13 18:56:00', '2020-01-13 18:56:00')
INSERT BookOrders VALUES (2, 6, '2080-01-13 18:56:00', '2081-01-13 18:56:00')
    

INSERT BookReservations VALUES
    (1, 5, '2020-01-13 18:56:00'),
    (1, 6, '2020-01-13 18:56:00'),
    (2, 5, '2020-01-13 18:56:00')

--SELECT * FROM dbo.GetClientBooks(1)
--SELECT * FROM dbo.GetBookClients(6)
--SELECT * FROM dbo.GetBookwormClient('1900-01-13 18:56:00', '2030-01-13 18:56:00')
--SELECT * FROM dbo.GetBookWithMaxCopies()
--SELECT * FROM dbo.GetBookWithMaxReservations()
--SELECT * FROM dbo.GetBookWithMaxClientsOnInterval('1945-01-13 18:56:00', '2000-01-13 18:56:00')
