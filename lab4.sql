﻿-- MS SQL 2014
USE master
DROP DATABASE EmilRakhimyanov
CREATE DATABASE EmilRakhimyanov
USE EmilRakhimyanov


CREATE TABLE Providers (
    id INT IDENTITY(1, 1) PRIMARY KEY,
    pricePerBrick FLOAT,
    discountPriceThreshold FLOAT,
    discount FLOAT,
    deliveryPrice FLOAT
)


GO
CREATE FUNCTION GetProviderPrice(@providerId INT, @bricks FLOAT) RETURNS FLOAT
BEGIN
    DECLARE @ppb FLOAT, @dpt FLOAT, @discount FLOAT, @dp FLOAT;
    SELECT
        @ppb = P.pricePerBrick,
        @discount = P.discount,
        @dpt = P.discountPriceThreshold,
        @dp = P.deliveryPrice
    FROM Providers P WHERE P.id = @providerId;
    RETURN (
        CASE WHEN (@ppb * @bricks > @dpt) THEN
            @ppb * @bricks + @dp * ((100 - @discount) / 100.0)
        ELSE
            @ppb * @bricks + @dp
        END
    )
END
GO


GO
CREATE FUNCTION GetBestProviders(@brickWeight FLOAT, @totalWeight FLOAT)
RETURNS @result TABLE (id INT, price FLOAT) AS
BEGIN
    DECLARE @bricks INT, @minPrice FLOAT
    SET @bricks = CEILING(@totalWeight / @brickWeight)
    SELECT @minPrice = MIN(dbo.GetProviderPrice(P.id, @bricks)) FROM Providers P
    INSERT @result SELECT id, @minPrice AS price FROM Providers
        WHERE dbo.GetProviderPrice(id, @bricks) = @minPrice
    RETURN
END
GO


GO
CREATE FUNCTION GetBreakPoint(@providerId INT) RETURNS FLOAT
BEGIN
    RETURN (SELECT (discountPriceThreshold / pricePerBrick) FROM Providers WHERE id = @providerId)
END
GO


GO
CREATE FUNCTION GetDiscountDelivery(@providerId INT) RETURNS FLOAT
BEGIN
    RETURN (SELECT ((100 - discount) / 100) * deliveryPrice FROM Providers WHERE id = @providerId)
END
GO


GO
CREATE FUNCTION GetSegments()
RETURNS @result TABLE (segmentS INT, segmentE INT) AS
BEGIN
    DECLARE @coords TABLE (coordX FLOAT)
    DECLARE @scoords TABLE (id INT IDENTITY(1, 1) PRIMARY KEY, coordX FLOAT)
    DECLARE @LENGTH INT
    SET @LENGTH = 1

    -- Здесь расставляются точки на оси X (кирпичи):
    INSERT @coords
        SELECT ABS(A.deliveryPrice - B.deliveryPrice) / ABS(A.pricePerBrick - B.pricePerBrick)
        FROM Providers A CROSS JOIN Providers B
        WHERE A.id != B.id

    INSERT @coords
        SELECT ABS(dbo.GetDiscountDelivery(A.id) - dbo.GetDiscountDelivery(B.id)) / ABS(A.pricePerBrick - B.pricePerBrick)
        FROM Providers A CROSS JOIN Providers B
        WHERE A.id != B.id

    INSERT @coords
        SELECT ABS(dbo.GetDiscountDelivery(A.id) - B.deliveryPrice) / ABS(A.pricePerBrick - B.pricePerBrick)
        FROM Providers A CROSS JOIN Providers B
        WHERE A.id != B.id

    INSERT @coords SELECT dbo.GetBreakPoint(id) FROM Providers
    INSERT @coords VALUES (0), (9999999)
    -- /////////////////////////////////////////////
    INSERT @scoords SELECT DISTINCT * FROM @coords ORDER BY coordX
    SELECT @LENGTH = COUNT(*) - 1 FROM @coords
    INSERT @result SELECT A.coordX, B.coordX FROM
        (SELECT * FROM @scoords ORDER BY coordX OFFSET 0 ROWS FETCH NEXT @LENGTH ROWS ONLY) A JOIN
        (SELECT * FROM @scoords ORDER BY coordX OFFSET 1 ROWS) B ON A.id + 1 = B.id
        ORDER BY A.coordX
    RETURN
END
GO


GO
CREATE FUNCTION GetProviderOnSegment(@segmentS INT, @segmentE INT) RETURNS INT
BEGIN
    DECLARE @minPrice FLOAT
    SELECT @minPrice = MIN(dbo.GetProviderPrice(id, (@segmentS+@segmentE) / 2 )) FROM Providers
    RETURN (SELECT TOP 1 id FROM Providers WHERE dbo.GetProviderPrice(id, (@segmentS+@segmentE) / 2) = @minPrice)
END
GO


GO
CREATE FUNCTION GetOptimalTable()
RETURNS @result TABLE (id INT, segmentS INT, segmentE INT, providerId INT) AS
BEGIN
    DECLARE @lastId INT, @lastProviderId INT
    DECLARE @tmp TABLE (id INT IDENTITY(1, 1) PRIMARY KEY, segmentS INT, segmentE INT, providerId INT)

    INSERT @tmp 
        SELECT segmentS, segmentE, dbo.GetProviderOnSegment(segmentS, segmentE) 
        FROM dbo.GetSegments() ORDER BY segmentS
    SELECT @lastId = COUNT(*) FROM @tmp
    SET @lastProviderId = -99999999
    -- Схлопывание соседних отрезков с одинаковыми продавцами:
    WHILE (@lastId > 0)
    BEGIN
        IF ((SELECT providerId FROM @tmp WHERE id = @lastId) = @lastProviderId)
        BEGIN
            WITH LR AS (SELECT TOP 1 * FROM @result ORDER BY id ASC)
            UPDATE LR SET segmentS = (SELECT segmentS FROM @tmp WHERE id = @lastId)
        END
        ELSE
        BEGIN
            INSERT @result SELECT * FROM @tmp WHERE id = @lastId
        END
        SELECT @lastProviderId = providerId FROM @tmp WHERE id = @lastId
        SET @lastId -= 1 
    END
    RETURN
END
GO


INSERT INTO Providers VALUES (10, 0, 0, 7000)
INSERT INTO Providers VALUES (12, 30000, 100, 6000)
INSERT INTO Providers VALUES (14, 33000, 70, 5500)


SELECT id AS N'Продавец', dbo.GetProviderPrice(id, 2700) AS N'Цена' FROM Providers
SELECT id AS N'Продавец', price AS N'Цена' FROM dbo.GetBestProviders(1, 2700)
-- ^ Эти селекты проверяют таблицу итоговую таблицу:
SELECT
    FORMATMESSAGE('%d-%d', CEILING(segmentS), CEILING(segmentE)) AS N'Количество кирпичей',
    providerId AS N'Продавец',
    dbo.GetProviderPrice(P.id, segmentS) AS N'Стартовая цена',
    dbo.GetProviderPrice(P.id, segmentE) AS N'Конечная цена'
    FROM dbo.GetOptimalTable() JOIN Providers P ON P.id = providerId
